.model small
.stack 100h
print macro s
    mov ah,9
    lea dx,s
    int 21h
endm
.data
    s db 13,'Kiem tra 2 xau S1 va S2 co bang nhau khong$'
    s1 db 10,13,'Huy Vo$'
    s2 db 10,13,'Huy Vo$'
    s3 db 10,13,'S1 va S2 bang nhau$'
    s4 db 10,13,'S1 va S2 khong bang nhau$'
.code

main proc
    mov ax,@data
    mov ds,ax
    lea si,s1 
    lea di,s2
    print s
    print s1
    print s2
    
check:
    mov al,[si]
    mov ah,[di]
    cmp al,'$'
    je checkah 
    cmp al,ah
    jne notequal
    inc si
    inc di 
    jmp check
    
checkah:
    cmp ah,'$'
    je equal   
    
equal:
    print s3
    jmp endcheck   
    
notequal:
    print s4
    jmp endcheck 
            
endcheck:     
    mov ah,4ch
	int 21h
    main endp
end main    